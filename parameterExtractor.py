# coding: utf-8
import logging
import re
import sys
from os.path import dirname, join, basename

from bs4 import BeautifulSoup

LOG_LEVEL = logging.DEBUG
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch = logging.StreamHandler(stream=sys.stdout)
ch.setLevel(LOG_LEVEL)
ch.setFormatter(formatter)
LOGGER = logging.getLogger("ParamExtractor")
LOGGER.addHandler(ch)
LOGGER.setLevel(LOG_LEVEL)

ROOT_DIR = dirname(__file__)
TAB = '  '


def get_full_path(args):
    return join(ROOT_DIR, *args)


def save_to_file(var_names, graph_name, output_dir=None):
    out_file_name = basename(graph_name).replace('.xml', '.yml')
    if not output_dir:
        output_dir = dirname(graph_name)

    output_file = join(output_dir, out_file_name)
    LOGGER.info('Writing output to %s', output_file)
    with open(output_file, "wt") as out_file:
        out_file.write('inputs:\n')
        for var_name in var_names.keys():
            out_file.write('{tab}- {var}\n'.format(var=var_name, tab=TAB))


def main(graph_file_path, output_dir=None):
    var_names = extract_var_names(graph_file_path)
    save_to_file(var_names, graph_file_path, output_dir)
    LOGGER.info('All done')


def extract_var_names(graph_file):
    var_names = {}
    search = re.compile('^\$\{(.*?)\}$')
    LOGGER.info('Extracting varaible names from %s', graph_file)
    with open(graph_file, "r") as in_file:
        soup = BeautifulSoup(in_file, "lxml-xml")
        txt_elts = soup.find_all(text=search)
        for txt_elt in txt_elts:
            match = search.match(txt_elt)
            if match:
                var_name = match.group(1)
                parent_name = txt_elt.parent.name
                operator_name = txt_elt.find_parent('node').find('operator').text
                var_names.update({var_name: [parent_name, operator_name]})
                LOGGER.debug('Variable name found by txt elt: %s', var_name)
                LOGGER.debug('Parent name =%s', parent_name)
                LOGGER.debug('Operator name =%s', operator_name)
    LOGGER.info('Variable names found: %s', var_names)
    return var_names


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description="""Script for extracting variable names to be replace in graph file"""
    )
    parser.add_argument('graph_file',
                        help="Full path to graph file")
    parser.add_argument('-output_dir', help="Output directory. Default to graph file directory")
    # graph_name = 'myGraphSnowSubsetReproj.xml'
    # graph_file = get_full_path([graph_name])
    args = parser.parse_args()
    main(args.graph_file, output_dir=args.output_dir)
